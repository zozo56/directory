package com.estm.directory.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.web.multipart.MultipartFile;

public class UserDTO {
	
	private Long id;
	
	//@NotNull
    //@Size(min = 1, message = "Firstname doit contenir zu moins '{min}' caractère")
    private String firstName;

    private String lastName;

    private String email;

    private String password;
    
    private String confirmpassword;
    
    private MultipartFile uploadingFile;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmpassword() {
		return confirmpassword;
	}

	public void setConfirmpassword(String confirmpassword) {
		this.confirmpassword = confirmpassword;
	}

	public MultipartFile getUploadingFile() {
		return uploadingFile;
	}

	public void setUploadingFile(MultipartFile uploadingFile) {
		this.uploadingFile = uploadingFile;
	}

}
