package com.estm.directory.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.estm.directory.dto.UserDTO;
import com.estm.directory.model.User;
import com.estm.directory.service.UserService;

@Controller
public class UserController {
	
	@Autowired
    private UserService userService;

    @GetMapping("/create")
	public String create(Model model) {
		model.addAttribute("userForm", new UserDTO());
		
		return "/create";
	}
    
    @PostMapping("/create")
	public String create(@Valid @ModelAttribute("userForm") UserDTO userForm, BindingResult bindingResult, Model model) {
		
    	
    	
    	User user = userService.createAnUser(userForm);
    	
    	// S'il y a des erreurs
    	if (!bindingResult.hasErrors() && user == null) {
    		// On affiche le formulaire
    		//return "/create";
    		model.addAttribute("userForm", userForm);
    		
    		return "/create";
    	}
    	
		return "redirect:/show/" + user.getId();
	}
    
    @GetMapping("/list")
	public String list(Model model) {
		model.addAttribute("users", userService.findAll());
		
		return "/list";
	}
    
    @GetMapping("/edit/{userId}")
	public String edit(@PathVariable Long userId, Model model) {
    	UserDTO userDto = new UserDTO();
    	Optional<User> userOp = userService.findById(userId);
    	
    	if (userOp.isPresent()) {
    		User user = userOp.get();
    		userDto.setId(user.getId());
    		userDto.setFirstName(user.getFirstName());
    		userDto.setEmail(user.getEmail());
    	}
    	
    	model.addAttribute("userForm", userDto);
		
		return "/edit";
	}
    
    @PostMapping("/edit/{userId}")
	public String edit(@Valid @ModelAttribute("userForm") UserDTO userForm, @PathVariable Long userId, BindingResult bindingResult, Model model) {
		
    	User user = userService.updateUser(userForm, userId);
    	
    	if (user == null) {
    		return "redirect:/edit/" + userId;
    	}
    	
		return "redirect:/show/" + user.getId();
	}
    
    @GetMapping("/show/{userId}")
	public String show(@PathVariable Long userId, Model model) {
    	Optional<User> userOp = userService.findById(userId);
    	
    	if (userOp.isPresent()) {
    		User user = userOp.get();
    		model.addAttribute("user", user);
    	}
    	
		return "/show";
	}
}
