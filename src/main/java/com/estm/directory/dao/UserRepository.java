package com.estm.directory.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.estm.directory.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);
}
