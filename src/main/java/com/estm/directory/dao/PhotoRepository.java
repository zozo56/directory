package com.estm.directory.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.estm.directory.model.Photo;

public interface PhotoRepository extends JpaRepository<Photo, Long> {
    
}
