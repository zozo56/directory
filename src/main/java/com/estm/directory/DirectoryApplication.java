package com.estm.directory;

import java.io.File;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.estm.directory.service.PhotoServiceImpl;

@SpringBootApplication
public class DirectoryApplication {

	public static void main(String[] args) {
		// Création du repertoire de photo
		new File(PhotoServiceImpl.uploadingDir).mkdirs();
		
		SpringApplication.run(DirectoryApplication.class, args);
	}

}
