package com.estm.directory.service;

import com.estm.directory.model.Photo;

public interface PhotoService {
	Photo save(Photo photo);
}
