package com.estm.directory.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.estm.directory.dao.PhotoRepository;
import com.estm.directory.model.Photo;

public class PhotoServiceImpl implements PhotoService {

	public static final String uploadingDir = System.getProperty("user.dir") + "/uploadingDir/";
	
	@Autowired
	PhotoRepository photoRepository;
	
	@Override
	public Photo save(Photo photo) {
		return photoRepository.save(photo);
	}

}
