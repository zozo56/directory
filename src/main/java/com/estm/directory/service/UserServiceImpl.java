package com.estm.directory.service;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.estm.directory.dao.PhotoRepository;
import com.estm.directory.dao.UserRepository;
import com.estm.directory.dto.UserDTO;
import com.estm.directory.model.Photo;
import com.estm.directory.model.User;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
    private UserRepository userRepository;
	
	@Autowired
	private PhotoRepository photoRepository;
	
	@Override
	public User save(User user) {
		return userRepository.save(user);
	}

	@Override
	public User createAnUser(UserDTO user) {
		User newUser = new User();
		
		if (user != null) {
			newUser.setFirstName(user.getFirstName());
			newUser.setEmail(user.getEmail());
			
			MultipartFile uploadedFile = user.getUploadingFile();
			if (uploadedFile != null) {
				File file = new File(PhotoServiceImpl.uploadingDir + uploadedFile.getOriginalFilename());
	            try {
					uploadedFile.transferTo(file);
				} catch (IllegalStateException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	            
	            Photo photo = new Photo();
	            photo.setFilename(file.getName());
	            photo.setPath(file.getPath());
	            photoRepository.save(photo);
	            
	            newUser.setPhoto(photo);
			}
			
			newUser = userRepository.save(newUser);
		}
		
		return newUser;
	}

	@Override
	public List<User> findAll() {
		return userRepository.findAll();
	}

	@Override
	public Optional<User> findById(Long id) {
		return userRepository.findById(id);
	}

	@Override
	public User updateUser(UserDTO user, Long id) {
		Optional<User> currentUserOp = userRepository.findById(id);
		if (currentUserOp.isPresent()) {
			User currentUser = currentUserOp.get();
			currentUser.setFirstName(user.getFirstName());
			currentUser.setEmail(user.getEmail());
			
			return userRepository.save(currentUser);
		}
		return null;
	}

}
