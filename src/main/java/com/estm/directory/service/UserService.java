package com.estm.directory.service;

import java.util.List;
import java.util.Optional;

import com.estm.directory.dto.UserDTO;
import com.estm.directory.model.User;

public interface UserService {
	User save(User user);
	User createAnUser(UserDTO user);
	User updateUser(UserDTO user, Long id);
	List<User> findAll();
	Optional<User> findById(Long id);
}
